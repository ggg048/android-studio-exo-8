package com.fessard.guillaume.applicationaudio;

import android.media.MediaPlayer;

import java.util.logging.Handler;

/**
 * Created by Guillaume Fessard on 25/01/2020.
 */
public class Musique extends Thread{

    private MediaPlayer musique;

    public Musique(MediaPlayer musique){
        this.musique = musique;
    }

    @Override
    public void run() {
        musique.start();
    }
}
