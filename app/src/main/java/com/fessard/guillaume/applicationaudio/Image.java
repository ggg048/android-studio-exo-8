package com.fessard.guillaume.applicationaudio;

import android.widget.ImageView;

/**
 * Created by Guillaume Fessard on 25/01/2020.
 */
public class Image extends Thread{

    private int possibledefilement=1;
    private int listeimage[] = {R.drawable.image1,R.drawable.image2};
    private ImageView image;


    public Image(ImageView image){
        this.image=image;
    }



    @Override
    public void run() {
        int i = 1;


        while(i>=0) {
            image.setImageResource(listeimage[i]);

            i--;
            try {
                Thread.sleep(1000);
            } catch (InterruptedException e) {
                e.printStackTrace();
            }

        }
    }

}
