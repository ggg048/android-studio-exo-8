package com.fessard.guillaume.applicationaudio;

import android.media.MediaPlayer;
import android.support.v7.app.AppCompatActivity;
import android.os.Bundle;
import android.view.View;
import android.widget.Button;
import android.widget.ImageView;



public class MainActivity extends AppCompatActivity {

    private MediaPlayer musique;
    private Button play;
    private Button pause;
    private ImageView imagemusique;

    @Override
    protected void onCreate(Bundle savedInstanceState) {
        super.onCreate(savedInstanceState);
        setContentView(R.layout.activity_main);

        play = (Button) findViewById(R.id.buttonplay);
        pause = (Button) findViewById(R.id.buttonpause);

        musique = MediaPlayer.create(this,R.raw.musique);

        imagemusique = (ImageView) findViewById(R.id.imagemusique);


        //Appuis sur le bouton play

        play.setOnClickListener(new View.OnClickListener() {
            @Override
            public void onClick(View v) {
                Musique ecoute = new Musique(musique);
                ecoute.run();

                defilementimage();

            }
        });

        //Appuis sur le bouton pause
        pause.setOnClickListener(new View.OnClickListener() {
            @Override
            public void onClick(View v) {
                musique.pause();
            }
        });
    }

    @Override
    public void onDestroy() {
        super.onDestroy();
        if (musique != null) musique.release();
    }

    private void defilementimage(){
        Image defilement = new Image(imagemusique);
        defilement.run();
    }


}
